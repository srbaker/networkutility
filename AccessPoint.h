/*
 * This file is part of NetworkUtility.
 *
 * NetworkUtility is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NetworkUtility is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NetworkUtility.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _ACCESS_POINT_H_
#define _ACCESS_POINT_H_

#include <Foundation/Foundation.h>

@interface AccessPoint : NSObject
{
  NSString *name;
  BOOL connected;
}

+(NSArray *)getAccessPoints;

-(NSString *)name;
-(void)name: (NSString *)newName;

-(BOOL)connected;
-(void)connected: (BOOL)isConnected;
@end

#endif /* _ACCESS_POINT_H_ */
