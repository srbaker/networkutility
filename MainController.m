/*
 * This file is part of NetworkUtility.
 *
 * NetworkUtility is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NetworkUtility is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NetworkUtility.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "MainController.h"
#include <Renaissance/Renaissance.h>

#import "AccessPoint.h"

@implementation MainController

-(void)applicationDidFinishLaunching: (NSNotification *)aNotification
{
  [NSBundle loadGSMarkupNamed: @"MainWindow"  owner: self];

  accessPointList = [AccessPoint getAccessPoints];
  [tableView reloadData];
}

-(int)numberOfRowsInTableView: (NSTableView *)aTableView
{
  return [accessPointList count];
}

-(id)tableView: (NSTableView *)aTableView objectValueForTableColumn: (NSTableColumn *)aTableColumn row:(int)rowIndex
{
  NSString *identifier = [aTableColumn identifier];
  AccessPoint *accessPoint = [accessPointList objectAtIndex: rowIndex];

  if ([identifier isEqual: @"wirelessNetworkConnected"])
    {
      if ([accessPoint connected])
        {
          return @"*";
        }
      else
        {
          return @"";
        }
    }
  else if ([identifier isEqual: @"wirelessNetworkName"])
    {
      return [accessPoint name];
    }

  return nil;
}

-(void)rescan
{
  accessPointList = [AccessPoint getAccessPoints];

  [tableView reloadData];
}

@end
