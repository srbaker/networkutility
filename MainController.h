/*
 * This file is part of NetworkUtility.
 *
 * NetworkUtility is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NetworkUtility is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NetworkUtility.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _APPLICATION_MAIN_CONTROLLER_H_
#define _APPLICATION_MAIN_CONTROLLER_H_

#include <Foundation/Foundation.h>
#include <AppKit/AppKit.h>
#include <Renaissance/Renaissance.h>

@interface MainController: NSObject
{
  IBOutlet NSTableView *tableView;
  NSArray *accessPointList;
}

- (void)applicationDidFinishLaunching: (NSNotification *)aNotification;

- (int)numberOfRowsInTableView: (NSTableView *)aTableView;
- (id)tableView: (NSTableView *)aTableView objectValueForTableColumn: (NSTableColumn *)aTableColumn row:(int)rowIndex;

@end

#endif /* _APPLICATION_MAIN_CONTROLLER_H_ */
